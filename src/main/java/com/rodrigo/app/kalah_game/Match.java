package com.rodrigo.app.kalah_game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Main class to run Kalah, manages user interface using Java console to test Game logic.
 * 
 * @author Drigo
 *
 */
public class Match {
	public static void main(String args[]){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		
		
		try {
			System.out.println("Player1's name:");
	        String p1 = br.readLine();
	        System.out.println("Player2's name:");
	        String p2 = br.readLine();
	        Game g = new Game(p1, p2);
	        g.printHelp();
	        br.readLine();
	        Boolean gContinues = true;
			while(gContinues){
				g.printBoard();
		        
		        System.out.println(g.getPlayerName()[g.getTurn()]+" Next move:");
		        
				String sPosition = br.readLine();
				if("help".equals(sPosition)){
					g.printHelp();
					br.readLine();
			        continue;
				}
				try{
					gContinues = g.move(Integer.parseInt(sPosition));
				} catch(NumberFormatException e){
					System.out.println("Invalid number, try again.");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
			
	}
	
}
