package com.rodrigo.app.kalah_game;

/**
 * @author Drigo
 *
 */
public class Game {
	private int[] board = {6,6,6,6,6,6,0,6,6,6,6,6,6,0};
	private int turn = 0;
	private String[] playerName = new String[2];
	private int winner = -1;
	
	/**
	 * @return the winner
	 */
	public int getWinner() {
		return winner;
	}

	public Game(){
		playerName[0]="Player1";
		playerName[1]="Player2";
	}	
	
	public Game(String player1, String player2){
		playerName[0]=player1;
		playerName[1]=player2;
	}
	
	/**
	 * @return the playerName
	 */
	public String[] getPlayerName() {
		return playerName;
	}

	/**
	 * @return the board
	 */
	public int[] getBoard() {
		return board;
	}

	/**
	 * @param board the board to set
	 */
	public void setBoard(int[] board) {
		this.board = board;
	}

	/**
	 * @return the turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * @param turn the turn to set
	 */
	public void setTurn(int turn) {
		this.turn = turn;
	}

	
	/**
	 * Sets Player1 name
	 * @param name
	 */
	public void setPlayer1Name(String name){
		playerName[0] = name;
	}
	
	/**
	 * Sets Player2's name
	 * @param name
	 */
	public void setPlayer2Name(String name){
		playerName[1] = name;
	}
	
	/**
	 * 
	 * Make a move from given pit position
	 * <p>
	 * Player1's turn is 0, his pits are in positions 0 - 5 and his storage is in position 6.<p>
	 * Player2's turn is 1, his pits are in positions 7 - 12 and his storage is in position 13.
	 * @param position
	 * The position where the player wants to make his move.
	 * @return
	 * true if game continues, false if game is over.
	 */
	public boolean move(int position) {
		if(!isValidMove(position)){
			System.out.println("Invalid move");
			return true;
		}
		int p = board[position];
		board[position]=0;
		int index=position;
		// p is the number of pieces to deal
		while(p>0){
			index++;
			//Skip opponent's storage!
			if(!isMine(index)&&isStorage(index)){
				index++;
			}
			//close the loop
			if(index>13){
				index=0;
			}
			board[index]=board[index]+1;
			p--;
		}
		processLastPiece(index);
		return !isGameOver();
	}

	
	/**
	 * Process the last piece move, to see if there is a free move or a steal.
	 * 
	 * @param position
	 */
	private void processLastPiece(int position){
		if(!isMine(position)){
			switchTurn();
			return;
		}
		if(isStorage(position)){
			System.out.println("Free move!");
			return;
		}
		if(board[position] == 1 ){
			System.out.println(board[getOpposite(position)] + " stolen!");
			board[getStorage()]+=board[getOpposite(position)]+1;
			board[position]=0;
			board[getOpposite(position)]=0;
		}
		switchTurn();
		
	}
	
	/**
	 * Retrieves the player storage depending on Player in turn
	 * @return
	 */
	private int getStorage(){
		return turn == 0 ? 6 : 13;
	}
	
	
	/**
	 * Retrieves the opposite pit position from given position
	 * @param position
	 * @return
	 */
	private int getOpposite(int position){
		return 12-position;
	}
	
	
	/**
	 * Switches turn between 1 and 0
	 */
	private void switchTurn(){
		turn = turn==0 ? 1 : 0;	
	}
	
	
	/**
	 * Indicates if given position is a storage position
	 * @param position
	 * @return
	 */
	private boolean isStorage(int position){
		return (position == 6 || position == 13);
	}
	
	
	/**
	 * Indicates if given position is from the player in turn side of the board
	 * @param position
	 * @return
	 */
	private boolean isMine(int position){
		if(position<=6){
			if(turn == 0){
				return true;
			}
		} else {
			if(turn == 1){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Validates move from given position, returns false if move is invalid, this could be because the given position is not on Payer's side,
	 * the pit is empty, given position is storage or invalid number. Position must be between 0 and 13.
	 * @param position
	 * @return
	 */
	private boolean isValidMove(int position){
		if(position < 0 ||position > 13){
			System.out.println("Invalid position, board goes from 0 to 13!");
			return false;
		}
		if( isStorage(position)){
			System.out.println("Storage position!");
			return false;
		}
		if((turn==0 && (position > 6 ))||(turn==1 && (position < 6 ))){
			System.out.println("Not your pit!");
			return false;
		}
		if(board[position]==0){
			System.out.println("Empty pit!");
			return false;
		}
		return true;
	}
	
	/**
	 * Indicates if the game is over. This is when one side of the board is empty.
	 * @return
	 * true if game is over.
	 */
	private boolean isGameOver(){
		boolean over = true;
		for(int i = 0 ; i<6 ; i++){
			if(board[i]!=0){
				over = false;
				break;
			}
		}
		if(!over){
			over=true;
			for(int i = 7 ; i<13 ; i++){
				if(board[i]!=0){
					over = false;
					break;
				}
			}
		}
		if(over){
			endGame();
		}
		return over;
		
	}
	
	/**
	 * Cleans the board and announces the winner
	 */
	private void endGame(){
		for(int i = 0 ; i<6 ; i++){
			board[6]+=board[i];
			board[i]=0;
		}
		for(int i = 7 ; i<13 ; i++){
			board[13]+=board[i];			
			board[i]=0;
		}
		System.out.println("Game over!");
		if(board[6]==board[13]){
			System.out.println("Draw!!");
		} else {
			winner=board[6]>board[13]?0:1;
			System.out.println(playerName[winner] + " won!");
		}
		printBoard();
		
	}
	
	/**
	 * Prints the board in console
	 * 
	 */
	public void printBoard(int[] b){
		System.out.print("\n\n\n    ");
		System.out.println("Player1: "+playerName[0] );
		System.out.print("    ");
		for(int i=5; i>=0 ; i-- ){
			System.out.print(b[i]+"   ");
		}
		System.out.println("\n\n"+b[6]+" - - - - - - - - - - - - - - "+b[13]+"\n");
		System.out.print("    ");
		for(int i=7; i<board.length-1 ; i++ ){
			System.out.print(b[i]+"   ");
		}
		System.out.println("\n\n    Player2: "+playerName[1] );
		System.out.println("\n");	
	}
	/**
	 * Prints the board in console
	 * 
	 */
	public void printBoard(){
		printBoard(board);
	}
	
	/**
	 * Prints user guide.
	 */
	public void printHelp(){
		System.out.println("Hi, this is your Kalah game.\nHere are the positions in the board, Player1 has positions 0-5"
				+"\nand his storage is in position 6, Player2 has positions 7-12 and his storage is in position 13.\n\n"
				+"Give the position where you want to make your move and type 'help' anytime to show this guide.\n\nGood luck!!!\n\nPositions board:");
		int[] positionsBoard = {0,1,2,3,4,5,6,7,8,9,10,11,12,13};
		printBoard(positionsBoard);
		System.out.println("Press enter to continue.");
		
	}
	
	
}
