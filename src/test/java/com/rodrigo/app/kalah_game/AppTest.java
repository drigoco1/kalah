package com.rodrigo.app.kalah_game;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for Kalah Game
 */
public class AppTest {
	@Test
	public void testExtraTurn() {
		Game g = new Game();
    	g.move(3);
    	g.move(7);
    	g.move(2);
    	g.move(8);
    	g.move(3);
    	g.move(7);
    	g.move(5);
    	g.move(9);
    	Assert.assertEquals(0, g.getTurn());
    	g.move(5);
		Assert.assertEquals(0, g.getTurn());
	}
	
	@Test
	public void testStealing() {
		Game g = new Game();
    	g.move(3);
    	g.move(7);
    	g.move(2);
    	g.move(8);
    	g.move(3);
    	int[] b = g.getBoard();
    	int steal = b[7]+b[4];
    	int storage = b[13];
    	g.move(7);
    	Assert.assertEquals(g.getBoard()[13], storage+steal );    	
	}
	
	/**
	 * Tests player1 trying to make a move from a pit 
	 * not on his side of the board
	 */
	@Test
	public void testNotYourPit() {
		Game g = new Game();
    	g.move(3);
    	int[] b1 = g.getBoard();
    	Assert.assertEquals(1, g.getTurn());
    	g.move(2);
    	Assert.assertEquals(1, g.getTurn());
    	Assert.assertArrayEquals(b1, g.getBoard());    	
	}
		
	/**
	 * Tests player trying to make a move from a out of range position
	 */
	@Test
	public void testInvalidPosition() {
		Game g = new Game();
    	g.move(3);
    	int[] b1 = g.getBoard();
    	Assert.assertEquals(1, g.getTurn());
    	g.move(14);
    	Assert.assertEquals(1, g.getTurn());
    	Assert.assertArrayEquals(b1, g.getBoard());    	
	}
		
	/**
	 * Tests player trying to make a move from a negative position
	 */
	@Test
	public void testNegativePosition() {
		Game g = new Game();
    	g.move(3);
    	int[] b1 = g.getBoard();
    	Assert.assertEquals(1, g.getTurn());
    	g.move(-9);
    	Assert.assertEquals(1, g.getTurn());
    	Assert.assertArrayEquals(b1, g.getBoard());    	
	}
		
	/**
	 * Tests player trying to make a move from the storage position
	 */
	@Test
	public void testStorage() {
		Game g = new Game();
    	g.move(3);
    	int[] b1 = g.getBoard();
    	Assert.assertEquals(1, g.getTurn());
    	g.move(13);
    	Assert.assertEquals(1, g.getTurn());
    	Assert.assertArrayEquals(b1, g.getBoard());    	
	}
		
	/**
	 * Tests player trying to make a move from an empty pit
	 */
	@Test
	public void testEmptyPit() {
		Game g = new Game();
		int[] newBoard = {0,0,0,0,0,5,31,0,0,0,0,0,1,35};
		g.setBoard(newBoard);
		Assert.assertEquals(0, g.getTurn());
    	g.move(2);
    	Assert.assertEquals(0, g.getTurn());
    	Assert.assertArrayEquals(newBoard, g.getBoard());    	
	}
	
	/**
	 * Tests behavior of a move leaving one side of the board empty.
	 * move method should return false as the game should not continue.
	 * Resulting in a draw
	 */
	@Test
	public void testGameEndDraw() {
		Game g = new Game();
		int[] newBoard = {0,0,0,0,0,5,31,0,0,0,0,0,1,35};
		g.setBoard(newBoard);
		g.setPlayer1Name("Player1");
		g.setPlayer2Name("Player2");
		g.setTurn(1);
    	Assert.assertEquals(false,g.move(12));
    	Assert.assertEquals(-1,g.getWinner());
	}
	
	/**
	 * Tests behavior of a move leaving one side of the board empty.
	 * move method should return false as the game should not continue.
	 * Resulting in Player2 Win
	 */
	@Test
	public void testGameEndP2Wins() {
		Game g = new Game();
		int[] newBoard = {0,0,0,0,0,5,30,0,0,0,0,0,1,36};
		g.setBoard(newBoard);
		g.setPlayer1Name("Player1");
		g.setPlayer2Name("Player2");
		g.setTurn(1);
		Assert.assertEquals(false,g.move(12));
		Assert.assertEquals(1,g.getWinner());
    }
	/**
	 * Tests behavior of a move leaving one side of the board empty.
	 * move method should return false as the game should not continue.
	 * Resulting in Player1 Win
	 */
	@Test
	public void testGameEndP1Wins() {
		Game g = new Game();
		int[] newBoard = {0,0,0,0,0,6,31,0,0,0,0,0,1,34};
		g.setBoard(newBoard);
		g.setPlayer1Name("Player1");
		g.setPlayer2Name("Player2");
		g.setTurn(1);
		Assert.assertEquals(false,g.move(12));
		Assert.assertEquals(0,g.getWinner());
    }
	
	/**
	 * Tests behavior of a move and switching turn to the other player
	 */
	@Test
	public void testTurnSwitch() {
		Game g = new Game();
		Assert.assertEquals(0, g.getTurn());
		g.move(3);
		Assert.assertEquals(1, g.getTurn());
		g.move(7);
		Assert.assertEquals(0, g.getTurn());
		
	}
	
	/**
	 * Tests behavior of a move going trough their own storage and to the next pits
	 */
	@Test
	public void testJumpToFirstPit() {
		Game g = new Game();
		int[] newBoard = {0,0,0,0,0,5,31,0,0,0,0,5,1,30};
		int[] expectedBoard = {1,1,1,0,0,5,31,0,0,0,0,0,2,31};
		g.setBoard(newBoard);
		g.setTurn(1);
		g.move(11);
		Assert.assertArrayEquals(expectedBoard, g.getBoard());    	
	}
	
	
	/**
	 * Tests behavior of a move skipping opponent's storage
	 */
	@Test
	public void testSkipOponentStorage() {
		Game g = new Game();
		int[] newBoard = {0,0,0,0,0,5,31,0,0,0,0,15,1,20};
		int[] expectedBoard = {1,1,1,1,1,6,31,1,1,1,1,1,3,22};
		g.setBoard(newBoard);
		g.setTurn(1);
		g.move(11);
		Assert.assertArrayEquals(expectedBoard, g.getBoard());    	
		
	}
	
	
}
